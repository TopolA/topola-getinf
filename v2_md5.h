#ifndef _V2_MD5_H
#define _V2_MD5_H 1

#include <unistd.h>
#include <inttypes.h>
#include <time.h>

#define MD5HashSize 16

typedef uint32_t uint32;
// typedef unsigned long uint32;

struct MD5Context {
    uint32 buf[4];
    uint32 bits[2];
    unsigned char in[64];
};

// typedef struct MD5Context MD5_CTX;

char *crypt_make_salt(void);
void byteReverse(unsigned char*, unsigned);
void MD5Init(struct MD5Context *ctx);
void MD5Update(struct MD5Context*, unsigned char const*, unsigned);
void MD5Final(unsigned char digest[16], struct MD5Context *ctx);
void MD5Transform(uint32 buf[4], uint32 const in[16]);
/* static void to64(char*, unsigned long, int); */
char *v2_md5_crypt(const char*, const char*);

/* V2 functions */
char *v2_pw_encrypt(const char *clear, const char *salt);
char *v2_make_md5_hash(time_t salt, char *clear, int is_arp);
int v2_md5_check_hash(const char *clear, const char *hash); // Check password
//int v2_check_hash(time_t salt, const char *clear, const char *hash);

// Mf5 sum functions
int v2_md5(const unsigned char *data, unsigned long dlen, unsigned char *md5_digest);
char *v2_md5_hex(unsigned char *md5_digest, int is_up);
char *v2_md5_sum(char *in_str);
char *v2_md5_sum_up(char *in_str);
char *v2_md5_sum_bin(char *in_str, unsigned in_len);
void v2_hmac_md5(const unsigned char *in_text, int text_len, const unsigned char *key, int key_len, unsigned char *digest);

#endif /* _V2_MD5_H */
