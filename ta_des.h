#ifndef DESH
#define DESH 1

#include <inttypes.h>
#define u32 uint32_t

// #define u32 unsigned long

typedef struct _des_ctx
    {
    u32 encrypt_subkeys[32];
    u32 decrypt_subkeys[32];
    }
des_ctx[1];

void DES_key_schedule(const unsigned char *, u32 *);
int DES_setkey(struct _des_ctx *, const unsigned char *);
int DES_ecb_crypt(struct _des_ctx *, const unsigned char *, unsigned char *, int);

/*
 * Handy macros for encryption and decryption of data
 */
#define DES_ecb_encrypt(ctx, from, to)    DES_ecb_crypt(ctx, from, to, 0)
#define DES_ecb_decrypt(ctx, from, to)    DES_ecb_crypt(ctx, from, to, 1)

#endif
