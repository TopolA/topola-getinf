/*
 *  Copyright (c) 2005-2015 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PR_PROT_H
#define _PR_PROT_H 1

#include <stdarg.h> // va_list

#include "ta_connect.h"
#include "ta_des.h"
#include "v2_md5.h"

#define PR_PROT_NUM 0x04 /* Protocol Number - old one = 4 */
#define PR_PROT_NUM1 0x05 /* Protocol Number - next generation = 5 */

#define PR_CONT_BIT 0x80 /* Has next packets */

#define PR_HDR_SIZE 8
#define PR_MAX_SIZE PR_MAX_BUFF_SIZE

/* Agent Commands */

#define TA_GET_SESSION  0x01 /* Request for session serve */
#define TA_GET_AUTH     0x02 /* Request for authorization */
#define TA_GET_CMD      0x20 /* Get Command */
#define TA_END_SESSION  0x7F /* End of session */

/* Remote Comands */

#define RM_WAIT_CONN     0x80 /* Wait for connect */
#define RM_AUTH_REQUEST  0x81 /* Autorization request */
#define RM_OK_CMD        0xC0 /* Send Answer */
#define RM_END_SESSION   0xFF /* End of session */

typedef struct pr_session_s {
    struct pr_session_s *next;

    pr_socket_t *soc;

    char *name;
    char *secret;
    time_t key;

    int pnum; // Protocol number 4 or 5
    int cont; // Received cont bit

    int (*pr_prs_fun)(char *);
    int (*pr_auth_fun)(char *);

    char *pr_log_file;

    time_t pr_rem_time;
} pr_session_t;

extern pr_session_t pr;

extern pr_socket_t *pr_soc;

extern int pr_prot_deb;

extern time_t pr_rem_time;

extern int (*pr_prs_fun)(char *);
extern int (*pr_auth_fun)(char *);

extern char *pr_log_file;
extern int pr_is_answ; // We are answering side...

extern int pr_last_ans; // Set or not cont bit at last command, i.e. lastest value of send pr_rcin_cont

char *ta_getcmd_name(int in_cod);
char *ta_make_hash(time_t salt, char *clear);
int ta_recv_cmd(uint8_t exp_cmd);
int ta_send_cmd(uint8_t in_cmd, char *cmd_str, int is_cont);
int ta_wait_session(char *ip, char *port);
int ta_get_session(char *ip, char *port);
int ta_ask_cmd(char *in_cmd, ...);
int ta_ask_cmd_cont(char *in_cmd, ...);
int ta_ans_ok(void); // ta_ans_cmd(0, "OK") if pr_rcin_cont == 0
int ta_ans_fin(void); // ta_ans_cmd(0, NULL) if pr_rcin_cont == 0
int ta_ans_cmd(int in_cont, char *in_cmd, ...);
int ta_add_log(int deb_num, char *msg, ...);

#endif // _PR_PROT_H
