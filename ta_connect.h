/*
 *  Copyright (c) 2005-2015 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TA_CONNECT_H
#define _TA_CONNECT_H 1

#include <stdlib.h>
#include <stdio.h>

#ifdef _HAS_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif

#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <inttypes.h>
//#include <fcntl.h> // mkdir()
#include <stdarg.h> // va_list
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <poll.h>
#include <libgen.h> /* basename() */

#include "v2_util.h"
#include "v2_err.h"

#define PR_MAX_BUFF_SIZE 4096

typedef struct pr_socket_s {
    int out_sock;       // stdout
    int inp_sock;        // stdin
    int rm_sock;      // netinout

    int proto;        // Protocol. Default IPPROTO_TCP. Allowed IPPROTO_SCTP

    int timeout;
    time_t lastop;

    uint8_t buf[PR_MAX_BUFF_SIZE]; // Cmd buffer

    char pr_str_error[MAX_STRING_LEN];
    int pr_elen; // Current end of the err_str

    //in_port_t sock_port; // Input port

    char *sock_port; // Local port
    char *sock_host; // IP address of local side

    char *peer_port; // IP address of remote port
    char *peer_host; // IP address of remote side

    char *ip_addr; // IP or host address
    char *ip_port;

    struct sockaddr_storage sock_addr;
    struct sockaddr_storage peer_addr;

    // External program instead tcp/ip connect
    char *cmd;
    pid_t pid; // PID of running process

} pr_socket_t;


/* ======================================================== */
int pr_soc_new(pr_socket_t **in_soc, char *ip, char *port);

int ta_make_connect(pr_socket_t *pr_soc);
int ta_close_connect(pr_socket_t *pr_soc);
int ta_get_peer(pr_socket_t *pr_soc);

int ta_send_buf(pr_socket_t *pr_soc);
int ta_recv_buf(pr_socket_t *pr_soc);

#endif // _TA_CONNECT_H
