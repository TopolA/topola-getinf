/*
 *  Copyright (c) 2005-2020 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ta_connect.h"
#include "v2_ip.h"

#include <sys/ioctl.h>
#include <sys/wait.h>

// getaddreinfo
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

/* =================================================== */
int pr_add_err(pr_socket_t *in_soc, char *format, ...) {
    char strtmp[MAX_STRING_LEN];
    int slen=0;

    VL_STR(strtmp, MAX_STRING_LEN, format);

    slen=strlen(strtmp);

    if(slen == 0) return(0); // Nothing to add - not typical one

    if((slen  == 1) && (strtmp[0] == '-')) { // Erase error string
	in_soc->pr_str_error[0] = '\0';
	in_soc->pr_elen         = 0;
	v2_del_error(); // Dell all errors from system list
	return(0);
    }

    if(slen && (strtmp[slen-1] == '\n')) strtmp[--slen] = '\0';

    in_soc->pr_elen+=snprintf(in_soc->pr_str_error+in_soc->pr_elen, MAX_STRING_LEN-in_soc->pr_elen, "%s%s", in_soc->pr_elen?"\n":"", strtmp);

    v2_add_error("%s", strtmp);

    return(0);
}
/* =================================================== */
int pr_soc_new(pr_socket_t **in_soc, char *ip, char *port) {

    if(!in_soc) return(1);

    if(!(*in_soc)) {
        if(!((*in_soc)=(pr_socket_t *)calloc(sizeof(pr_socket_t), 1))) return(2);
    }

    if(ip && *ip=='/') { // Means local call
	v2_let_var(&(*in_soc)->cmd, ip);
    } else {
	v2_let_var(&(*in_soc)->ip_addr, ip);
	v2_let_var(&(*in_soc)->ip_port, port);
    }

    (*in_soc)->out_sock = STDOUT_FILENO; // stdout
    (*in_soc)->inp_sock = STDIN_FILENO;  // stdin
    (*in_soc)->rm_sock  = 0;             // network in-out sock

    (*in_soc)->proto    = IPPROTO_TCP;   // Or IPPROTO_SCTP

    (*in_soc)->timeout  = 80;            // timeout

    (*in_soc)->pid      = (pid_t)0;      // PID of sub-process

    return(0);
}
/* =================================================== */
// Run external program as remote peer - experimental!!!
int ta_make_pipe(pr_socket_t *pr_soc) {
    int fn_ta[2]; // Pipe from agent - read   fn_ta[0] <- fn_ta[1]
    int to_ta[2]; // Pipe to agent   - write  to_ta[1] -> to_ta[0]
    char **arg_v=NULL;

    if(!v2_is_val(pr_soc->cmd)) return(140); // Special case when ip_add AKA cmd == "-"

    if(pipe(fn_ta) == -1) return(141);
    if(pipe(to_ta) == -1) return(142);

    pr_soc->pid=fork();

    if(pr_soc->pid == (pid_t)-1) return(143);

    if(pr_soc->pid != (pid_t)0) { // Parent

	close(to_ta[0]); // Child read
	close(fn_ta[1]); // Child write

	pr_soc->inp_sock=fn_ta[0]; // Parent read
	pr_soc->out_sock=to_ta[1]; // Parent write

	return(0);
    }


    // Child

    close(fn_ta[0]); // Parent read
    close(to_ta[1]); // Parent write


    dup2(to_ta[0], STDIN_FILENO);  // Child read
    dup2(fn_ta[1], STDOUT_FILENO); // Child out


    close(to_ta[0]); // Child read
    close(fn_ta[1]); // Child write

    if(!(arg_v=v2_argv(pr_soc->cmd))) return(144); // allocation error

    execv(*arg_v, arg_v); // If returned - always = -1 + errno

    pr_add_err(pr_soc, "Pipe: exec problem[%d]: %s", errno, strerror(errno));
    return(149);
}
/* =================================================== */
int ta_make_connect(pr_socket_t *pr_soc) {
    struct addrinfo a_hint;
    struct addrinfo *a_host=NULL;
    struct addrinfo *a_temp=NULL;
    char *p=NULL;
    int rc=0;

    pr_add_err(pr_soc, "-"); // Reset error

    if(v2_is_par(pr_soc->cmd)) return(ta_make_pipe(pr_soc));

    if(!v2_is_par(pr_soc->ip_addr)) return(101);
    if(!v2_is_par(pr_soc->ip_port)) return(102);

    memset(&a_hint, 0, sizeof(struct addrinfo));
    a_hint.ai_family   = AF_UNSPEC; // AF_INET, AF_INET6
    a_hint.ai_socktype = SOCK_STREAM;
    a_hint.ai_flags    = AI_NUMERICSERV; // 0; // AI_CANONNAME = Add cannonical name
    a_hint.ai_protocol = pr_soc->proto; // IPPROTO_TCP or IPPROTO_SCTP

    rc=getaddrinfo(pr_soc->ip_addr, pr_soc->ip_port, &a_hint, &a_host);
    if(rc != 0) {
	pr_add_err(pr_soc, "GetAddrInfo[ %d = %s ]: |%s:%s|", rc, gai_strerror(rc), v2_nn(pr_soc->ip_addr), v2_nn(pr_soc->ip_port));
        return(105);
    }

    for(a_temp=a_host; a_temp != NULL; a_temp=a_temp->ai_next) {
	pr_soc->rm_sock=socket(a_temp->ai_family, a_temp->ai_socktype, a_temp->ai_protocol);
	if(pr_soc->rm_sock==-1) continue; // A bit strange....

	rc=connect(pr_soc->rm_sock, a_temp->ai_addr, a_temp->ai_addrlen);
	if(rc != -1) break; // OK - exit

	v2_ip_var(&p, (struct sockaddr_storage*)a_temp->ai_addr);
    }

    if(a_temp == NULL) rc=106;
    freeaddrinfo(a_host);

    if(rc) pr_add_err(pr_soc, "Connect to %s:%s [%d]: %s", v2_st(p, "-"), pr_soc->ip_port, errno, strerror(errno));

    v2_freestr(&p);

    if(rc) return(rc);

    pr_soc->out_sock=pr_soc->rm_sock;
    pr_soc->inp_sock=pr_soc->rm_sock;

    return(0);
}
/* =================================================== */
int ta_close_connect(pr_socket_t *pr_soc) {
    int rc=0;

    if(pr_soc->pid) {
	waitpid(pr_soc->pid, &rc, 0);
	if(!WIFEXITED(rc)) {
	    pr_add_err(pr_soc, "Pipe: Child process ran unsuccessfuly[%d]: %s", errno, strerror(errno));
	    return(141);
	}
	if(WEXITSTATUS(rc)) {
	    pr_add_err(pr_soc, "Pipe: Child process returned code %d", rc);
	    return(142);
	}
    }


    if(!pr_soc->rm_sock) return(0); // Non ip connection


    if(shutdown(pr_soc->rm_sock, SHUT_RDWR)) { // Shutdown now
	pr_add_err(pr_soc, "Close connect, shutdown[%d]: %s", errno, strerror(errno));
	return(108);
    }
    if(close(pr_soc->rm_sock)) { // Close sock
	pr_add_err(pr_soc, "Close connect[%d]: %s", errno, strerror(errno));
	return(109);
    }
    return(0);
}
/* ==================================================== */
// Dirty Hack - doesn't check real things - only guessing :)
int ta_get_peer(pr_socket_t *pr_soc) {
    socklen_t svlen=sizeof(struct sockaddr_storage);

    // Clear R/T buffer
    memset(pr_soc->buf, 0, PR_MAX_BUFF_SIZE);

    // First be sure we got with the socket as input
    memset(&pr_soc->sock_addr, 0, svlen);
    if (getsockname(STDIN_FILENO, (struct sockaddr *)&pr_soc->sock_addr, &svlen) == -1)  return(0); // Not socket operations
    if (!v2_ip_var(&pr_soc->sock_host, &pr_soc->sock_addr)) return(0); // OK - Not ipv4 or ipv6 socket
    v2_let_varf(&pr_soc->sock_port, "%d", v2_ip_port(&pr_soc->sock_addr));

    memset(&pr_soc->peer_addr, 0, svlen);
    if (getpeername(STDIN_FILENO, (struct sockaddr *)&pr_soc->peer_addr, &svlen) == -1) { // Not socket operations
	pr_add_err(pr_soc, "GetPeerName[%d]: %s", errno, strerror(errno));
	return(1); // We can not to get know who is on another side
    }

    // We accept only inet ip4 and ipv6 calls, no other
    if (!v2_ip_var(&pr_soc->peer_host, &pr_soc->peer_addr)) return(3); // Unknown remote host
    v2_let_varf(&pr_soc->peer_port, "%d", v2_ip_port(&pr_soc->peer_addr));

    return(0); // OK - We have peer in peer_host
}
/* =================================================== */
int ta_send_buf(pr_socket_t *pr_soc) {
    struct pollfd ufd;
    uint16_t buf_len=0;
    int res=0;

    pr_soc->lastop=time(NULL);

    buf_len=pr_soc->buf[0] + pr_soc->buf[1] * 256;
    if((buf_len < sizeof(buf_len)) || (buf_len > PR_MAX_BUFF_SIZE)) return(110);

    /* Prepare to output Poll */
    ufd.fd=pr_soc->out_sock;
    ufd.events=POLLOUT;

    if((res=poll(&ufd, 1, pr_soc->timeout*1000)) == -1) {
	pr_add_err(pr_soc, "Send Poll[%d]: %s", errno, strerror(errno));
        return(104);
    }

    res=write(pr_soc->out_sock, pr_soc->buf, buf_len);
    if(res==-1) {
	pr_add_err(pr_soc, "Send bytes[%d]: %s", errno, strerror(errno));
        return(105);
    }
    if(res != buf_len) {
	pr_add_err(pr_soc, "Sent %d bytes instead %d", res, buf_len);
        return(112);
    }
    return(0);
}
/* =================================================== */
int ta_recv_buf(pr_socket_t *pr_soc) {
    struct pollfd ufd;
    int res=0;
    uint16_t red_len=0;
    uint16_t in_len=2;   // First bytes to read
    unsigned int nfds=1; // Number of FDs
    int n_read=0;

    pr_soc->lastop=time(NULL);

    errno=0;

    memset(pr_soc->buf, 0, PR_MAX_BUFF_SIZE);

    while(red_len<in_len) {

	ufd.fd=pr_soc->inp_sock;
        ufd.events=POLLIN;

	res=poll(&ufd, nfds, pr_soc->timeout*1000);

	if(res == -1) { // -1 - means error
	    pr_add_err(pr_soc, "Read Poll[%d]: %s", errno, strerror(errno));
            return(120);
	}

	if(res == 0) { // 0 - means timeout
	    pr_add_err(pr_soc, "Read Poll Timeout");
            return(130);
	}

	if(res > nfds) { // too many FDs, never happes due nfds == 1 (redundant remark code)
	    pr_add_err(pr_soc, "Read Poll Too many descriptions number");
            return(131);
	}

	if(ufd.revents&POLLHUP) return(0); // Returns end of file

	if(!(ufd.revents&POLLIN)) {
	    pr_add_err(pr_soc, "Read Poll Unknown returned event: %0x", ufd.revents);
	    return(132);
	}

	if(ioctl(pr_soc->inp_sock, FIONREAD, &n_read) == -1) {
	    pr_add_err(pr_soc, "Read IOCTL[%d]: %s", errno, strerror(errno));
            return(133);
	}

	if(n_read == 0) {
	    pr_add_err(pr_soc, "Nothing to read, dead end.");
            return(124);
	}

	res=read(pr_soc->inp_sock, pr_soc->buf+red_len, in_len-red_len);

	if(res == -1) { // Error
	    pr_add_err(pr_soc, "Read[%d]: %s", errno, strerror(errno));
            return(121);
	}

        // End of file (shutdown connection)
	if(res == 0) { // Duplicates FIONREAD ioctl (redundant code)
	    pr_add_err(pr_soc, "Red 0 bytes (unconnected?).");
            return(126);
	}

	// pr_add_err(pr_soc, "Received len: %d (%s)", res, pr_soc->buf+8);
	//v2_add_warn("Received len: %d (%s)", res, pr_soc->buf+8);

        red_len+=res;
        if(red_len==2) { // First itteration
	    in_len=pr_soc->buf[0] + pr_soc->buf[1] * 256;

	    //v2_add_warn("In len == %d", in_len);

	    if(in_len < sizeof(in_len))   return(122);
	    if(in_len > PR_MAX_BUFF_SIZE) return(123);
        }
    }
    return(0);
}
/* ============================================================ */
