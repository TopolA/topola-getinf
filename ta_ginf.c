/*
 *  Copyright (c) 2005-2015 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ta_ginf.h"
#include "v2_lstr.h"
#include <errno.h>

str_lst_t **ret_pstr=NULL; // If set - do not print result, just return list into that variable

str_lst_t *ta_pstr=NULL;
str_lst_t *ta_errr=NULL;

char *tag_host=NULL;
char *tag_port=NULL;

/* ====================================================== */
int tag_read_conf(void) {
    //FILE *cf=NULL;
    char strtmp[MAX_STRING_LEN];
    str_lst_t *cfg=NULL;
    str_lst_t *str_tmp=NULL;
    char *tconf=v2_st(getenv("TAGINF_CONF"), TAGINF_CONFIG);
    long main_key=0;
    int rc=0;

    if((rc=v2_lstr.fcfg(&cfg, tconf))) {
	v2_lstr.tailf(&ta_errr, "Can't open config file \"%s\": rc=%d", tconf, rc);
	v2_lstr.tailf(&ta_errr, "%s", strerror(errno));
	v2_lstr.tail(&ta_errr, "#################");
	v2_lstr.tail(&ta_errr, "name id");
        v2_lstr.tail(&ta_errr, "code password");
        v2_lstr.tail(&ta_errr, "key number");
        v2_lstr.tail(&ta_errr, "host IP");
        v2_lstr.tail(&ta_errr, "port TCP-port");
        v2_lstr.tail(&ta_errr, "debug level");
	v2_lstr.tail(&ta_errr, "#################");
	v2_lstr.tail(&ta_errr, "### OR ###");
	v2_lstr.tail(&ta_errr, "#################");
	v2_lstr.tail(&ta_errr, "taginf_name id");
        v2_lstr.tail(&ta_errr, "taginf_code password");
        v2_lstr.tail(&ta_errr, "taginf_key number");
        v2_lstr.tail(&ta_errr, "taginf_host IP");
        v2_lstr.tail(&ta_errr, "taginf_port TCP-port");
	v2_lstr.tail(&ta_errr, "#################");
	v2_lstr.tail(&ta_errr, "### OR ###");
	v2_lstr.tail(&ta_errr, "#################");
	v2_lstr.tail(&ta_errr, "taginf_access id:number:password");
        v2_lstr.tail(&ta_errr, "taginf_host IP");
        v2_lstr.tail(&ta_errr, "taginf_port TCP-port");
	v2_lstr.tail(&ta_errr, "#################");
	v2_lstr.tailf(&ta_errr, "Var TAGINF_CONF = %s", v2_nn(getenv("TAGINF_CONF")));
        return(8);
    }

    FOR_LST(str_tmp, cfg) {

	if(v2_lstr.cf_str(str_tmp, &pr.name,     "taginf_name"))  continue;
	if(v2_lstr.cf_str(str_tmp, &pr.secret,   "taginf_code"))  continue;

	if(v2_lstr.cf_str(str_tmp, &tag_host,    "taginf_host"))  continue;
	if(v2_lstr.cf_str(str_tmp, &tag_port,    "taginf_port"))  continue;

	if(v2_lstr.cf_lng(str_tmp, &main_key,    "taginf_key"))   continue;
	if(v2_lstr.cf_int(str_tmp, &pr_prot_deb, "taginf_debug")) continue;
	if(v2_lstr.cf_int(str_tmp, &pr_prot_deb, "debug"))        continue;

	if(!str_tmp->str) continue; // Need to param

	if(!v2_strcmp(str_tmp->key, "taginf_access")) {
	    if(!v2_getword(strtmp, str_tmp->str, ':')) continue;
	    v2_let_if_var(&pr.name, strtmp);

	    if(!v2_getword(strtmp, str_tmp->str, ':')) continue;
	    if(!main_key) main_key=atol(strtmp);

	    if(!v2_getword(strtmp, str_tmp->str, ' ')) continue;
	    v2_let_if_var(&pr.secret, strtmp);
	    continue;
	}

	// Direct names, if not set yet
	if(!v2_strcmp(str_tmp->key, "name")) {v2_let_if_var(&pr.name,   str_tmp->str); continue;}
	if(!v2_strcmp(str_tmp->key, "code")) {v2_let_if_var(&pr.secret, str_tmp->str); continue;}

	if(!v2_strcmp(str_tmp->key, "host")) {v2_let_if_var(&tag_host,   str_tmp->str); continue;}
	if(!v2_strcmp(str_tmp->key, "port")) {v2_let_if_var(&tag_port,   str_tmp->str); continue;}

	if(!v2_strcmp(str_tmp->key, "key"))  {
	    if(!main_key) main_key=strtol(str_tmp->str, NULL, 0);
	    continue;
	}
    }

    pr.key=(time_t)main_key;

    v2_lstr.free(&cfg); // Free config file

    if(!pr.name) {
	v2_lstr.tail(&ta_errr, "Empty name");
        rc += 1;
    }

    if(!pr.secret) {
	v2_lstr.tail(&ta_errr, "Empty code");
        rc += 2;
    }

    if(!pr.key) {
	v2_lstr.tail(&ta_errr, "Empty key");
        rc += 4;
    }

    v2_let_if_var(&tag_host, "localhost"); // Default host, if not set
    v2_let_if_var(&tag_port, "4766");      // Default port, if not set

    if(rc) rc += 20;

    return(rc);
}
/* ====================================================== */
int ta_parse_tag(char *in_cmd) {

    if(!in_cmd) return(0);
    if(!in_cmd[0]) return(0);

    if(!strncmp(in_cmd, "OK", 2)) {
	v2_lstr.tail(&ta_pstr, in_cmd);
    } else if(!strncmp(in_cmd, "STR:", 4)) {
	v2_lstr.tail(&ta_pstr, in_cmd+4);
    } else if(!strncmp(in_cmd, "REM:", 4)) {
	v2_lstr.tailf(&ta_pstr, "### %s", in_cmd+4);
    } else if(!strncmp(in_cmd, "ER:", 3)) {
	v2_lstr.tailf(&ta_errr, "Error: %s", in_cmd+3);
	//return(1);
    } else if(!strncmp(in_cmd, "WR:", 3)) {
	v2_lstr.tailf(&ta_errr, "Warning: %s", in_cmd+3);
        return(1);
    } else {
	v2_lstr.tailf(&ta_pstr, "CMD: %s", in_cmd);
    }

    return(0);
}
/* ======================================================================= */
int tag_prn_error(int in_rc) {
    str_lst_t *str_tmp=NULL;

    FOR_LST(str_tmp, ta_errr) {
        printf("### %s\n", str_tmp->key);
    }
    if(in_rc) printf("### RC=%d\n", in_rc);

    return(in_rc);
}
/* ======================================================================= */
int ta_call_taginf(int arg_c, char *arg_v[]) {
    str_lst_t *str_tmp=NULL;
    char strtmp[MAX_STRING_LEN];
    char *cmdname=NULL;
    int rc=0;
    int x;
    int is_quiet=0;

    if(getenv("TAGINF_QUIET")) is_quiet=1;

    if(!pr_prs_fun) pr_prs_fun=&ta_parse_tag;

    if((rc=tag_read_conf())) {
        return(tag_prn_error(rc));
    }

    if((rc=ta_get_session(tag_host, tag_port))) {
	if(pr_soc && v2_is_par(pr_soc->pr_str_error)) {
	    v2_lstr.tailf(&ta_errr, "TA prot[err=%d]: %s", rc, pr_soc->pr_str_error);
	} else if(rc==255) {
	    v2_lstr.tailf(&ta_errr, "TA prot[err=%d]: End Session", rc);
	} else {
	    v2_lstr.tailf(&ta_errr, "TA prot[err=%d]: Returned code %d", rc, rc);
	}
        return(tag_prn_error(rc));
    }

    cmdname=basename(arg_v[0]);

    // Make command string
    rc=0;
    if(!strncmp(cmdname, "tagi_", 5)) rc=sprintf(strtmp, "%s", cmdname+5);
    for(x=1; x<arg_c; x++) {
	if(!v2_strcmp(arg_v[x], "-t")) continue; // Skip "taremote -t arg ..." part
	rc+=sprintf(strtmp+rc, "%s%s", rc?" ":"", arg_v[x]);
    }

    if(!rc) rc=sprintf(strtmp, "help");

    if(!strncmp(cmdname, "tagi_", 5) || !strcmp(cmdname, "taginf") || !strcmp(cmdname, "taremote")) {
	if(!(rc=ta_ask_cmd("%s", strtmp))) ta_ask_cmd("-");
    } else {
        if(!(rc=ta_ask_cmd("help"))) ta_ask_cmd("-");
    }

    if(ret_pstr) {
	*ret_pstr = ta_pstr;
    } else {
	FOR_LST(str_tmp, ta_pstr) {
	    if(is_quiet) {
		if(!v2_strcmp(str_tmp->key, "OK"))        continue;
		if(!v2_strcmp(str_tmp->key, "status OK")) continue;
	    }
	    printf("%s\n", str_tmp->key);
	}
    }

    tag_prn_error(rc);

    return(rc);
}
/* ======================================================================= */
