/*
 *  Copyright (c) 2005-2015 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ta_prot.h"

pr_session_t pr; // Session
pr_socket_t *pr_soc=NULL;

int pr_prot_deb=0;

int (*pr_prs_fun)(char *)=NULL;
int (*pr_auth_fun)(char *)=NULL;

char *pr_log_file=NULL;

time_t pr_rem_time=0;

int pr_is_answ=0;

/* ======================================= */
time_t pr_auth_hash=0;
char *pr_name_str=NULL;
char *pr_auth_str=NULL;
uint8_t pr_d_key[8];

int pr_is_crypted=0;

int pr_is_remote=0;

char pr_str_deb[MAX_STRING_LEN];

int pr_cmd=0;
int pr_last_ans=0; // Set or not cont bit at last command

time_t pr_rcin_time=0;
time_t pr_rcou_time=0;

/* =================================================== */
char *ta_getcmd_name(int in_cod) {
    static char strtmp[32];
    if(in_cod == TA_GET_SESSION)  return("TA_GET_SES");
    if(in_cod == TA_GET_AUTH)     return("TA_GET_AUT");
    if(in_cod == TA_GET_CMD)      return("TA_GET_CMD");
    if(in_cod == TA_END_SESSION)  return("TA_END_SES");
    if(in_cod == RM_WAIT_CONN)    return("RM_WAIT_CN");
    if(in_cod == RM_AUTH_REQUEST) return("RM_AUTH_RQ");
    if(in_cod == RM_OK_CMD)       return("RM_OKK_CMD");
    if(in_cod == RM_END_SESSION)  return("RM_END_SES");
    sprintf(strtmp, "PR_CMD_%02X", in_cod);
    return(strtmp);
}
/* =================================================== */
char *ta_make_hash(time_t salt, char *clear) {
    char salt_str[20];
    char *res=NULL;
    char *p;

    sprintf(salt_str, "$1$%08lx", (unsigned long)salt);
    res=v2_pw_encrypt(clear, salt_str);
    p=strrchr(res, '$');
    return(++p);
}

/* =================================================== */
int ta_crypt_data(int is_decrypt) {
    uint16_t pk_len=0;
    uint8_t recovered[8];
    des_ctx context;
    int i;

    pk_len=pr_soc->buf[0] + pr_soc->buf[1] * 256;
    if(!is_decrypt) {
        if(pk_len % 8) pk_len = (pk_len/8 + 1)*8;
        pr_soc->buf[0] = pk_len % 256;
        pr_soc->buf[1] = pk_len / 256;
    }

    DES_setkey(context, pr_d_key);

    for (i=8; i<pk_len; i += 8) {
        DES_ecb_crypt(context, pr_soc->buf+i, recovered, is_decrypt);
        memcpy(pr_soc->buf+i, recovered, 8);
    }
    return(0);
}
/* =================================================== */
void ta_set_key(time_t salt) {
    char *auth=ta_make_hash(salt, pr.secret);

    pr_is_crypted=1;

    pr_d_key[0] = auth[2];
    pr_d_key[1] = auth[7];
    pr_d_key[2] = auth[1];
    pr_d_key[3] = auth[5];
    pr_d_key[4] = auth[12];
    pr_d_key[5] = auth[9];
    pr_d_key[6] = auth[3];
    pr_d_key[7] = auth[11];
    return;
}
/* =================================================== */
// Returns protocol string
char *ta_pr_str(pr_socket_t *in_soc) {
    if(in_soc) return((char*)in_soc->buf+PR_HDR_SIZE);
    return(NULL);
}
/* =================================================== */
int ta_recv_cmd(uint8_t exp_cmd) {
    uint16_t in_len=0;
    int rc=0;

    if((rc=ta_recv_buf(pr_soc))) return(rc);

    pr.pnum = pr_soc->buf[2] & ~PR_CONT_BIT; // Register protocol number

    if((pr.pnum != PR_PROT_NUM) && (pr.pnum != PR_PROT_NUM1)) {
	ta_add_log(0, "Wrong protocol signature (strangers call) %d != %d || %d", pr_soc->buf[2], PR_PROT_NUM, PR_PROT_NUM1);
	//ta_add_log(0, "Packet: %02x%02x", pr_soc->buf[0], pr_soc->buf[1]);
	return(631);
    }
    pr_cmd=pr_soc->buf[3];
    pr.cont=0;
    if(pr_soc->buf[2] & PR_CONT_BIT) pr.cont=1;

    pr_rcin_time=pr_soc->buf[4] + pr_soc->buf[5] * 256
        + pr_soc->buf[6] * 256 * 256 + pr_soc->buf[7] * 256 * 256 * 256;

    in_len=pr_soc->buf[0] + pr_soc->buf[1] * 256;

    if(in_len > 8) {
        if(pr_is_crypted) ta_crypt_data(1);
	ta_add_log(3, "r%c [%s] (%d) \"%s\"", (pr.cont?'=':'<'),
                ta_getcmd_name(pr_soc->buf[3]), in_len, pr_soc->buf+PR_HDR_SIZE);
    } else {
        ta_add_log(3, "r%c [%s] (%d)", (pr.cont?'=':'<'),
                ta_getcmd_name(pr_soc->buf[3]), in_len);
    }

    if(exp_cmd == 0) return(0); // Wait for any command

    if((exp_cmd == TA_GET_SESSION) && (pr_cmd == RM_WAIT_CONN)) return(30); // Just return code

    if(pr_cmd == TA_END_SESSION) return(255); // End of calls
    if(pr_cmd == RM_END_SESSION) return(255); // End of calls

    if(exp_cmd == pr_cmd) return(0); // Got correct command

    if(pr_cmd & 0x80) { // RM_ cmd
	ta_send_cmd(TA_END_SESSION, NULL, 0);
    } else { // TA_ cmd
	ta_send_cmd(RM_END_SESSION, NULL, 0);
    }

    return(32);
}
/* =================================================== */
int ta_send_cmd(uint8_t in_cmd, char *cmd_str, int is_cont) {
    uint16_t cmd_len=0;
    int rc=0;

    pr_rcou_time=time(NULL);

    memset(pr_soc->buf, 0, PR_MAX_SIZE);
    pr_soc->buf[3] = in_cmd;
    pr_soc->buf[2] = PR_PROT_NUM;
    if(is_cont) pr_soc->buf[2] |= PR_CONT_BIT;

    // indian independly
    pr_soc->buf[4] = pr_rcou_time % 256;
    pr_soc->buf[5] = (pr_rcou_time / 256) % 256;
    pr_soc->buf[6] = (pr_rcou_time / 256 / 256) % 256;
    pr_soc->buf[7] = pr_rcou_time / 256 / 256 / 256;

    if(cmd_str && cmd_str[0]) {
        if((cmd_len=strlen(cmd_str) + PR_HDR_SIZE + 1) > PR_MAX_SIZE) return(90);

        pr_soc->buf[0] = cmd_len % 256;
        pr_soc->buf[1] = cmd_len / 256;

	memcpy(pr_soc->buf+PR_HDR_SIZE, cmd_str, strlen(cmd_str)+1);
	ta_add_log(3, "s%c [%s] (%d) \"%s\"", (is_cont?'=':'>'), ta_getcmd_name(pr_soc->buf[3]),
                cmd_len, pr_soc->buf+PR_HDR_SIZE);
        if(pr_is_crypted) ta_crypt_data(0);
    } else {
        cmd_len=PR_HDR_SIZE;

	pr_soc->buf[0] = cmd_len % 256;
        pr_soc->buf[1] = cmd_len / 256;

	ta_add_log(3, "s> [%s] (0)", ta_getcmd_name(pr_soc->buf[3]));
    }

    if((rc=ta_send_buf(pr_soc))) return(rc);

    if(in_cmd == TA_END_SESSION) {
	if((rc=ta_close_connect(pr_soc))) {
	    //ta_add_log(0, "Can't close connect to remote host, code=%d", rc);
	    return(35);
	}
	ta_add_log(2, "Session closed");
    }

    return(0);
}
/* =================================================== */
// Answer to call
int ta_wait_session(char *ip, char *port) {
    char *hash_inpt=NULL;

    int is_denid=0;
    int res=0;
    int rc=0;

    if(pr_soc_new(&pr_soc, ip, port)) return(79);

    pr_is_remote=1;
    pr_is_crypted=0;

    ta_add_log(2, "Start Wait session");

    if(pr_soc->peer_host) { // Answer call - auth fun should be present
        if(!pr_auth_fun) return(74);
    } else { // Outgoing call - secret should be
        if(!pr.name || !pr.secret)                     return(71);
        if((rc=ta_make_connect(pr_soc)))               return(rc);
        if((rc=ta_send_cmd(RM_WAIT_CONN, pr.name, 0))) return(rc);
    }

    if((rc=ta_recv_cmd(TA_GET_SESSION))) {
	// Oops... Does it income session?
	if(pr_soc->peer_host && (pr_cmd == RM_WAIT_CONN)) {
	    pr_is_answ=1;
	    return(ta_get_session(NULL, NULL));
	}
	return(rc);
    }
    if(pr_soc->peer_host) {
	if(!v2_let_var(&pr.name, ta_pr_str(pr_soc))) return(72);
    }

    if(!pr.cont) {
	if((rc=ta_send_cmd(RM_AUTH_REQUEST, NULL, 0))) return(rc);
    }

    if((rc=ta_recv_cmd(TA_GET_AUTH))) return(rc);
    hash_inpt=ta_pr_str(pr_soc);

    if(pr_soc->peer_host) {
	res=pr_auth_fun(pr.name);
    }

    if(pr.key == 0) is_denid|=0x02; // Not actually needs
    if(!pr.secret)  is_denid|=0x04; // Not actually needs
    if(is_denid) {
	ta_add_log(0, "ERROR: Auth init [%d] [%x]", res, is_denid);
        if((rc=ta_send_cmd(RM_END_SESSION, NULL, 0))) return(rc);
        return(79);
    }

    pr_auth_str=ta_make_hash(pr_rcou_time+pr.key, pr.secret);

    if(strcmp(pr_auth_str, hash_inpt)) {
	ta_add_log(0, "ERROR: Wrong auth hash Local: %s Remote: %s", pr_auth_str, hash_inpt);
	if((rc=ta_send_cmd(RM_END_SESSION, NULL, 0))) return(rc);
	return(73);
    }

    ta_set_key(pr_rcou_time);

    if(!pr_prs_fun) {
	ta_send_cmd(RM_END_SESSION, "ER:Parser not defined", 0);
	return(76);
    }

    if(res) {
	char s[400];
	if((rc=ta_send_cmd(RM_END_SESSION, v2_s(s, 400, "Ret code: %d", res), 0))) return(rc);
	return(78);
    }

    if((rc=ta_send_cmd(RM_OK_CMD, NULL, 0))) return(rc);

    while(!(rc=ta_recv_cmd(TA_GET_CMD))) {
	if((rc=pr_prs_fun(v2_let_var(NULL, (char *)(pr_soc->buf+PR_HDR_SIZE))))) {
	    ta_send_cmd(RM_END_SESSION, NULL, 0);
	    return(rc);
	}
    }
    if(!pr_soc->peer_host) {
	ta_add_log(2, "Try to close wait session");
	ta_close_connect(pr_soc);
    }
    return(rc);
}
/* =================================================== */
// Call to remote
int ta_get_session(char *ip, char *port) {
    int is_denid=0;
    int rc=0;

    if(pr_soc_new(&pr_soc, ip, port)) return(79);

    pr_is_crypted=0;

    if(pr_soc->peer_host) {
        if(!pr_auth_fun) return(84);
	if(!pr_is_answ) { // If not received wait conn yet
	    if((rc=ta_recv_cmd(RM_WAIT_CONN))) return(rc);
        }
        if(!v2_let_var(&pr.name, (char *)(pr_soc->buf+PR_HDR_SIZE))) return(83);
        if(pr_auth_fun(pr.name)) is_denid=1;
        pr_is_answ=0; // ???
        if((rc=ta_send_cmd(TA_GET_SESSION, NULL, 0))) return(rc);
    } else {
        if(!pr.name || !pr.secret)                       return(82);
        if((rc=ta_make_connect(pr_soc)))                 return(rc);
	if((rc=ta_send_cmd(TA_GET_SESSION, pr.name, 0))) return(rc);
    }

    if((rc=ta_recv_cmd(RM_AUTH_REQUEST))) return(rc);

    pr_rem_time=pr_rcin_time;
    if(is_denid) {
        pr_auth_hash=pr_rcin_time+getpid();
        pr_auth_str=ta_make_hash(pr_auth_hash, "wrong");
    } else {
        pr_auth_hash=pr_rcin_time;
        pr_auth_str=ta_make_hash(pr_rcin_time+pr.key, pr.secret);
    }
    if((rc=ta_send_cmd(TA_GET_AUTH, pr_auth_str, 0))) return(rc);

    ta_set_key(pr_auth_hash);

    while(!(rc=ta_recv_cmd(RM_OK_CMD)) && pr.cont) { // Receive extra data
	// Print buffer
    }
    return(rc);
}
/* =================================================== */
int ta_ask_cmd_cont(char *in_cmd, ...) {
    char strcmd[MAX_STRING_LEN];
    int rc=0;

    VL_STR(strcmd, MAX_STRING_LEN, in_cmd);

    if(in_cmd && (in_cmd[0] == '-') && (in_cmd[1] == '\0')) {
        if((rc=ta_send_cmd(TA_END_SESSION, NULL, 0))) return(rc);
        return(0);
    }

    // Dont wait answer commads
    return(ta_send_cmd(TA_GET_CMD, strcmd, 1));
}
/* =================================================== */
int ta_ask_cmd(char *in_cmd, ...) {
    char strcmd[MAX_STRING_LEN];
    int rc=0;

    VL_STR(strcmd, MAX_STRING_LEN, in_cmd);

    if(in_cmd && (in_cmd[0] == '-') && (in_cmd[1] == '\0')) {
        if((rc=ta_send_cmd(TA_END_SESSION, NULL, 0))) return(rc);
        return(0);
    }

    if((rc=ta_send_cmd(TA_GET_CMD, strcmd, 0))) return(rc);
    do {
        if((rc=ta_recv_cmd(RM_OK_CMD))) {
	    //if(rc!=255) ta_send_cmd(TA_END_SESSION, NULL, 0);
        } else if(pr_prs_fun) {
            if((rc=pr_prs_fun(v2_let_var(NULL, (char *)(pr_soc->buf+PR_HDR_SIZE))))) {
                ta_send_cmd(TA_END_SESSION, NULL, 0);
            }
        }
    } while((rc==0) && (pr_soc->buf[2] & PR_CONT_BIT));
    return(rc);
}
/* =================================================================== */
int ta_ans_ok(void) {
    if(pr.cont) return(0); // Wait for next value
    return(ta_ans_cmd(0, "OK"));
}
/* =================================================================== */
int ta_ans_fin(void) {
    if(pr.cont) return(0); // Wait for next value
    return(ta_ans_cmd(0, NULL));
}
/* =================================================== */
int ta_ans_cmd(int in_cont, char *in_cmd, ...) {
    char strcmd[MAX_STRING_LEN];
    int rc=0;

    VL_STR(strcmd, MAX_STRING_LEN, in_cmd);

    pr_last_ans=in_cont;

    if((rc=ta_send_cmd(RM_OK_CMD, strcmd, in_cont))) {
        ta_add_log(0, "Can't send cmd \"%s\", rc=%d", strcmd, rc);
        ta_add_log(0, "Error: \"%s\"", pr_soc->pr_str_error);
        ta_send_cmd(RM_END_SESSION, NULL, 0);
    }
    return(rc);
}
/* =================================================== */
int ta_add_log(int deb_num, char *msg, ...) {
    FILE *lg=NULL;
    char strmsg[MAX_STRING_LEN];
    str_lst_t *str_tmp=NULL;
    int res=0;

    VL_STR(strmsg, MAX_STRING_LEN, msg);
    if(!strmsg[0]) sprintf(strmsg, "-=MARK=-");

    if((res=strlen(strmsg))) {
	if(strmsg[res-1] == '\n') strmsg[res-1]='\0';
    }

    if((!pr_soc->peer_host) && (pr_prot_deb>2)) {
	printf("%s\n", strmsg);
	v2_prn_error();
	v2_lstr_free(&v2_err_lst);
    }

    if(deb_num > pr_prot_deb) return(0);

    if(!pr_log_file) return(0);

    if(!lg) {
	if(!(lg=fopen(pr_log_file, "a"))) return(1);
    }

    fprintf(lg, "%s [%s, %s] %5d : %s\n", v2_get_date(0),
	    v2_st(pr_soc->peer_host, "-"),
	    v2_st(pr.name, "-"),
	    (int)getpid(), strmsg);


    FOR_LST(str_tmp, v2_err_lst) {
	fprintf(lg, "%s", v2_get_date(str_tmp->a_time));
	fprintf(lg, " [%s, %s] %5d : ",
		v2_st(pr_soc->peer_host, "-"),
		v2_st(pr.name, "-"),
		(int)getpid());

	if(str_tmp->b_time==0) {
	    fprintf(lg, "ERROR");
	} else if(str_tmp->b_time==1) {
	    fprintf(lg, "WARN!");
	} else {
	    fprintf(lg, "DEBUG");
	}
	if(str_tmp->num) {
	    fprintf(lg, " [RC=%d]", str_tmp->num);
	}
	fprintf(lg, " %s\n", v2_nn(str_tmp->key));
    }

    v2_lstr_free(&v2_err_lst);

    return(fclose(lg));
}
/* =================================================== */
