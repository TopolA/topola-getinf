/* Created automatically TopolA Version file
TOPOLA_VERSION 5.37.21
*/

#ifndef _TPA_VERS_H
#define _TPA_VERS_H 1
#define TOPOLA_VERSION "5.37.21"
#define TOPOLA_VERSBIN "053721"
#define TOPOLA_VERSINT 53721
#endif // _TPA_VERS_H
