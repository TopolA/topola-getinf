CC=gcc
CFLAGS= -O2 -Wall

DEFS=

PROGNAME=taginf

SOURCES := $(wildcard *.c)
OBJ := $(patsubst %.c, %.o, $(SOURCES))

#OBJ= $(PROGNAME).o ta_prot.o ta_connect.o ta_des.o ta_md5.o\
#  ta_pstr.o v2_util.o

.c.o:
	$(CC) -c $(CFLAGS) $(DEFS) $<

all: $(PROGNAME)

$(PROGNAME): $(OBJ) Makefile Makefile.dep
	$(CC) $(CFLAGS) $(DEFS) -o $(PROGNAME) $(OBJ)

Makefile.dep:
	echo \# > Makefile.dep

version.h: tpa_vers.h
	@echo \#ifndef _VERSION_H > version.h
	@echo \#define _VERSION_H 1 >> version.h
	@LANG=C echo \#define USTAT_COMPILE_TIME \"`date +%d-%b-%Y`\" >> version.h
	@LANG=C echo \#define USTAT_COPY_TIME \"`date +%Y`\" >> version.h
	@echo \#include '"'tpa_vers.h'"' >> version.h
	@echo \#endif // _VERSION_H >> version.h

clean:
	rm -f *.o *.cgi *~ core *.b $(PROGNAME) version.h

dep: clean version.h
	$(CC) -MM *.c > Makefile.dep


include Makefile.dep
