/*
 *  Copyright (c) 2005-2006 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This file is part of the TaRemote util which is a remote
 *  agent of the TopolA managment software.
 *
 *  TaRemote is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  TaRemote is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar; if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 *  Boston, MA  02111-1307 USA.
 */

#ifndef _TAGINF_H
#define _TAGINF_H 1

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h> // basename();

#ifdef _HAS_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif

#include "version.h"
#include "ta_prot.h"
// #include "ta_pstr.h"
#include "ta_ginf.h"

#define TAGINF_VER "TaGetInfo v"USTAT_VERSION

#define TAGINF_CONFIG ".taginf"
//#define TAGI_DEBUG 0


#endif // _TAGINF_H
