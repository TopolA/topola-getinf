/*
 *  Copyright (c) 2005-2015 Oleg Vlasenko <vop@unity.net>
 *  All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PR_GINF_H
#define _PR_GINF_H 1

#include "ta_prot.h"

extern str_lst_t **ret_pstr; // If set - do not print result, just return list into that variable

/* ====================================================== */
#define TAGINF_CONFIG ".taginf"

int ta_call_taginf(int arg_c, char *arg_v[]);

/* ====================================================== */

#endif // _PR_GINF_H
